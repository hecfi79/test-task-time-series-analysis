import requests  # для пользователя без опыта: обязательно читайте README.md
import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error, mean_absolute_error


class Analysis(object):
    def __init__(self, symbol: str = "BTCUSDT", interval: str = "1h", limit: int = 1000) -> None:  # загрузка данных с использованием Binance API
        self.model, self.scaler, self.scalered_data = None, None, None
        self.train_data, self.test_data, self.predictions = None, None, None
        base_url = "https://api.binance.com/api/v3/klines"
        url = f"{base_url}?symbol={symbol}&interval={interval}&limit={limit}"
        response = requests.get(url)
        data = response.json()
        self.df = pd.DataFrame(data, columns=["Open_time", "Open", "High", "Low", "Close", "Volume", "Close_time",
                                              "Quote_asset_volume", "Number_of_trades", "Taker_buy_base_asset_volume",
                                              "Taker_buy_quote_asset_volume", "Ignore"])  # конвертация в DataFrame
        self.df.to_csv("binance_data.csv", index=False) # сохранение данных

    def preparing_data(self) -> None:  # предобработка данных
        self.df["Open_time"] = pd.to_datetime(self.df["Open_time"], unit="ms")
        self.scaler = MinMaxScaler()  # масштабирование цен
        self.scalered_data = self.scaler.fit_transform(self.df["Close"].values.reshape(-1, 1))

    def pca(self) -> None:
        pca = PCA(n_components=3)
        X = self.df.drop(['Open_time'], axis=1).values
        pcas = pca.fit_transform(X)
        new = pd.DataFrame(pcas, columns=['Feature 1', 'Feature 2', 'Feature 3'], index=self.df.index)
        new_df = pd.concat([self.df["Open_time"], new], axis=1)
        new_df = new_df[:len(new_df) - 1]
        self.df = new_df

    def train_test_split(self, data: np.ndarray, train_size: float = 0.8) -> None:  # разделение данных на обучающую и тестовую выборки
        train_len = int(len(data) * train_size)
        self.train_data = data[:train_len]
        self.test_data = data[train_len:]

    @staticmethod
    def create_windows(data: np.ndarray, window: int = 60) -> tuple:  # cоздание временных окон для LSTM
        X, y = [], []
        for i in range(window, len(data)):
            X.append(data[i - window:i, 0])
            y.append(data[i, 0])
        X, y = np.array(X), np.array(y)
        X = np.reshape(X, (X.shape[0], X.shape[1], 1))
        return X, y

    def build_train_lstm(self, window: int = 60) -> None:  # создание и обучение LSTM-модели
        X_train, y_train = self.create_windows(self.train_data, window)
        self.model = tf.keras.Sequential([
            tf.keras.layers.LSTM(100, return_sequences=True, input_shape=(X_train.shape[1], 1)),
            tf.keras.layers.LSTM(50, return_sequences=False),
            tf.keras.layers.Dense(25),
            tf.keras.layers.Dense(12),
            tf.keras.layers.Dense(1)
        ])
        self.model.compile(optimizer="adam", loss="mean_squared_error", metrics=["mae"])
        self.model.fit(X_train, y_train, batch_size=16, epochs=32)

    def evaluate_model(self, window: int = 60) -> tuple:  # оценка качества модели
        X_test, y_test = self.create_windows(self.test_data, window)
        self.predictions = self.model.predict(X_test)
        self.predictions = self.scaler.inverse_transform(self.predictions)
        y_test_inv = self.scaler.inverse_transform(y_test.reshape(-1, 1))
        mse = mean_squared_error(y_test_inv, self.predictions)
        mae = mean_absolute_error(y_test_inv, self.predictions)
        return mse, mae, y_test_inv

    def visualization(self, y_test_inv: np.ndarray) -> None:  # визуализация
        plt.figure(figsize=(14, 6))
        plt.plot(y_test_inv, label="Actual Prices", color="blue")
        plt.plot(self.predictions, label="Predicted Prices", color="red")
        plt.title("Cryptocurrency Price Prediction")
        plt.xlabel("Time")
        plt.ylabel("Price")
        plt.legend()
        plt.show()

    def main_method(self, pca: bool = False) -> None: # основной метод работы программы
        self.preparing_data()
        if pca:
            self.pca()
        self.train_test_split(self.scalered_data)
        self.build_train_lstm()
        mse, mae, y_test_inv = self.evaluate_model()
        print("Mean Squared Error:", mse)
        print("Mean Absolute Error:", mae)
        self.visualization(y_test_inv)


print("Результат работы без метода главных компонент:")
Analysis().main_method(pca=False)  # вызов без PCA
print("Результат работы с помощью метода главных компонент:")
Analysis().main_method(pca=True)  # вызов с PCA
