# Test task time series analysis

## Инструкция по запуску проекта для неопытных пользователей

### Если у Вас Windows 10 и выше:

- Переходим на страницу с установкой [PyCharm Community Edition](https://www.jetbrains.com/pycharm/download/) и выбираем версию Community;
- Последнее окно установки: обязательно поставьте галочку на Add To Path (команда pip работать не будет);
- Запускаем app.py с помощью PyCharm;
- В PyCharm на левой боковой панели снизу есть кнопки. Выберите терминал. Его значок будет выглядеть примерно [так](https://cdn.discordapp.com/attachments/983750986122293259/1098615581248344225/image.png);
- Далее в открывшейся панели вбиваем очередь команд:
> pip install requests  
> pip install pandas  
> pip install numpy  
> pip install tensorflow  
> pip install scikit-learn
- Случай, если не работает команда pip (либо не ставили галочку во втором пункте, либо уже давно было все установлено и забыто):  
Для лояльных к английскому пользователям: https://www.youtube.com/watch?v=dj5oOPaeIqI or https://www.youtube.com/watch?v=xhGWpnyVK8c&pp=ygUgcGlwIGNvbW1hbmQgbm90IGZvdW5kIHdpbmRvd3MgMTA%3D  
Для тех, кто в английский не может: https://www.youtube.com/watch?v=aL-4bSPWU04  
И повторяем предыдущий пункт после перезапуска ПК;
- Далее нажимаем на кнопку запуска (как правило, ее видно, большая зеленая стрелка где-то сверху, но если ее нет, то нажимаем правой кнопкой мыши на свободное место в коде и кнопку Run);
- Проект запущен, результаты в binance_data.csv будут обновлены ввиду постоянного обновления на сайте Binance.

### Если у Вас Linux:

Если Вы не ладите с английским, то прочтите это перед началом работы:  
[Инструкция по установке PyCharm Community на Linux](https://losst.pro/ustanovka-pycharm-ubuntu-16-04)  
[Инструкция по установке Python 3](https://losst.pro/ustanovka-python-3-ubuntu)  

Если же с английским все в порядке, то рекомендую посетить эти сайты:  
[PyCharm Community Installation](https://www.digitalocean.com/community/tutorials/install-pycharm-on-linux)  
[Python 3 Installation](https://www.geeksforgeeks.org/how-to-download-and-install-python-latest-version-on-linux/)  

Далее делаем следующее:

- Запускаем app.py с помощью PyCharm;
- В PyCharm на левой боковой панели снизу есть кнопки. Выберите терминал. Его значок будет выглядеть примерно [так](https://cdn.discordapp.com/attachments/983750986122293259/1098615581248344225/image.png);
- Далее в открывшейся панели вбиваем очередь команд:
> pip install requests  
> pip install pandas  
> pip install numpy  
> pip install tensorflow  
> pip install scikit-learn  
- Далее нажимаем на кнопку запуска (как правило, ее видно, большая зеленая стрелка где-то сверху, но если ее нет, то нажимаем правой кнопкой мыши на свободное место в коде и кнопку Run);
- Проект запущен, результаты в binance_data.csv будут обновлены ввиду постоянного обновления на сайте Binance.

## В случае возникновения вопросов и проблем

Мои контакты:
- ruslangulyanov.live@gmail.com
- [VK](https://vk.com/hecfi)
- [Kwork](https://kwork.ru/user/ruslangulyanovlive) (если Вы не клиент, укажите это в сообщении)
- Discord: Hecfi#4004
